import socket
import time
import logging
import sys

HOST = '127.0.0.1'
PORT = 5000
TIMEOUT = 5
BUF_SIZE = 1024


class WhatsUpClient():

    def __init__(self, host=HOST, port=PORT):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))
        logging.info('Connecting to %s:%s' % (host, port))
        try:
            path = "/bar"
            cmd =('GET %s HTTP/1.0\r\n\r\n' % path).encode()
            self.sock.send(cmd)
        except:
            self.sock.close()

        buf = []
        while True:
            chunk = self.sock.recv(1000)
            if not chunk:
                break
            buf.append(chunk)

        self.sock.close()
        print((b''.join(buf)).decode().split('\n')[0])

    def run(self):
        pass


def main():
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s] %(levelname)s: %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S %p')
    client = WhatsUpClient()

if __name__ == '__main__':
    main()
